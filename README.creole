= jmp-acct_bot =

An XMPP bot that allows users of https://jmp.chat/ (and any other instance of
jmp-register and jmp-fwdcalls) to perform various actions on their account,
including viewing usage and getting and settings plan and payment options.

The canonical location for the jmp-acct_bot source code is
https://gitlab.com/ossguy/jmp-acct_bot .

You can run jmp-acct_bot like this (last parameters picks the "stg" config):

 REDIS_URL=redis://127.0.0.1:6379/0 /usr/bin/bundle exec ./jmp-acct_bot.rb stg

A settings file must be placed in the current directory at
settings-<config>_acct_bot.rb - it must define the following variables:

* LOGIN_USER (the JID that the bot uses)
* LOGIN_PWD (the password for the bot's JID)
* CHEOGRAM_JID (the JID of the Cheogram instance that this JMP instance uses)
* SUPPORT_JID (the support JID for the JMP instance being used)
* PLANS_FILE (absolute path to file containing PLANS dictionary; detail in code)
* STATS_BEGIN_DAY (first YYYYMMDD for which stats (usage_* keys) are available)


Copyright (C) 2018  Denver Gingerich <denver@ossguy.com>

Copying and distribution of this README.creole file, with or without
modification, are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
