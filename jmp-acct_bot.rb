#!/usr/bin/env ruby
#
# Copyright (C) 2014-2020  Denver Gingerich <denver@ossguy.com>
#
# This file is part of jmp-acct_bot.
#
# jmp-acct_bot is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# jmp-acct_bot is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with jmp-acct_bot.  If not, see <http://www.gnu.org/licenses/>.

require 'blather/client/dsl'
require 'em-hiredis'
require 'em-http-request'
require 'webrick'
require 'securerandom'

require_relative 'em_promise'

if ARGV.size != 1 then
	puts "Usage: jmp-acct_bot.rb <type>"
	exit 0
end

TYPE_NAME = ARGV[0]

load "settings-#{TYPE_NAME}_acct_bot.rb"  # LOGIN_*, SUPP*/CHEO*_JID, PLANS_FILE
load PLANS_FILE
load V2CREDS_FILE

# use default host and port (implied by the JID) if user didn't define them
LOGIN_HOST = nil unless defined? LOGIN_HOST
LOGIN_PORT = nil unless defined? LOGIN_PORT

# TODO: keep in sync with jmp-fwdcalls.rb/sgx-catapult.rb&put in common location
module CatapultSettingFlagBits
	VOICEMAIL_TRANSCRIPTION_DISABLED = 0
	MMS_ON_OOB_URL = 1			# previously used, now inactive
end


module AccountBot
	extend Blather::DSL

	def self.log(msg)
		t = Time.now
		puts "LOG %s %d.%09d: %s" % [t.utc.iso8601, t.to_i, t.nsec, msg]
		STDOUT.flush
	end

	setup LOGIN_USER, LOGIN_PWD, LOGIN_HOST, LOGIN_PORT, nil, nil,
		workqueue_count: 0

	when_ready do
		log "Account bot connected - send messages to #{jid.stripped}."
	end

	def self.fetch_catapult_cred_for(jid)
		# TODO: XEP-0106 Sec 4.3 compliance; won't work with pre-escaped
		cheo_jid = jid.stripped.to_s.
			gsub("\\", "\\\\5c").
			gsub(' ', "\\\\20").
			gsub('"', "\\\\22").
			gsub('&', "\\\\26").
			gsub("'", "\\\\27").
			gsub('/', "\\\\2f").
			gsub(':', "\\\\3a").
			gsub('<', "\\\\3c").
			gsub('>', "\\\\3e").
			gsub('@', "\\\\40") + '@' + CHEOGRAM_JID

		cred_key = "catapult_cred-#{cheo_jid}"
		REDIS.lrange(cred_key, 0, 3).then { |creds|
			if creds.length < 4
				''
			else
				creds
			end
		}
	end

	def self.dollars(centicents)
		"$%.2f" % (centicents.to_f / 10000)
	end

	def self.current_plans_text(this_plan_key, next_plan_key)
		this_plan = PLANS[this_plan_key]
		next_plan = PLANS[next_plan_key]

		rv = ''

		if this_plan_key == next_plan_key
			rv += "Your plan for this month and going forward: "
		else
			rv += "Your plan for this month: "
		end

		rv += "'#{this_plan['name']}', which is in "

		if 'CAD' == this_plan['currency']
			rv += "Canadian dollars"
		elsif 'USD' == this_plan['currency']
			rv += "US dollars"
		else
			log "unexpected currency #{this_plan['currency']} for "\
				"user with above JID/number"

			# TODO: make better error/warning
			rv += "an unknown currency"
		end

		rv += " and costs #{dollars(this_plan['monthly_price'])}/month"\
			" - it includes "

		if -1 == this_plan['included_messages']
			rv += "unlimited messages "
		else
			rv += this_plan['included_messages'].to_s +
				" outgoing messages (with extra at "\
				"#{dollars(this_plan['extra_per_message'])}"\
				"/message - unlimited incoming) "
		end

		# TODO: if add unlimited minutes, MUST change below

		rv += "and #{this_plan['included_minutes']} minutes (with "\
			"extra at #{dollars(this_plan['extra_per_minute'])}"\
			"/minute)"

		if this_plan_key != next_plan_key
			rv += "\n\nYour new plan starting next month: TODO"
					#"\nNext month your plan will "\
					#"be #{next_plan_key} unless "\
					#"you switch before it starts"
			# TODO: fill in details; MUST do before plans can change
		end

		return rv
	end

	def self.payment_info(jid)
		REDIS.get('payment-funding_method-' + jid).then { |fund_method|
			if fund_method.nil? or fund_method.empty?
				log "nil or empty funding method for '#{jid}'"

				# TODO: error case after jmp-register sets fmthd
				"Your account is currently in trial mode so "\
				"no payment method has been set yet - see "\
				"https://jmp.chat/#upgrade to upgrade to a "\
				"paid account, or contact us at xmpp:"\
				"#{SUPPORT_JID} if you're unsure how much time"\
				" is left in your trial period"
			elsif 'prepaid' == fund_method
				log "prepaid funding method for '#{jid}'"

				"Your account is being paid from your prepaid "\
				"balance; to see when your next payment is "\
				"due, refer to your last communication with "\
				"xmpp:#{SUPPORT_JID} or contact us at xmpp:"\
				"#{SUPPORT_JID} - we normally respond within "\
				"24 hours"\
				"\n\n"\
				"To be notified once we've added real-time "\
				"balance information here, join our group chat"\
				" at xmpp:discuss@conference.soprani.ca?join "\
				"or our email notification list at "\
				"https://jmp.chat/sp1a/notify_signup/"
			elsif fund_method.start_with? 'recurring_paypal'
				log "PayPal funding method for '#{jid}'"

				"Your account is being paid via your PayPal "\
				"subscription - for details, see the JMP part "\
				"of https://www.paypal.com/myaccount/autopay/ "\
				"- feel free to contact us at xmpp:"\
				"#{SUPPORT_JID} if you have any questions"
			# TODO: add more clauses here, eg. when non-PayPal added
			else
				log "unknown funding method for '#{jid}'"

				# TODO: should be unknown in this case
				"Unknown payment method - please contact us at"\
					" xmpp:#{SUPPORT_JID} to resolve this"
			end
		}
	end

	def self.trial_text(jmp_number)
		REDIS.get('usage_start_day-' + jmp_number).then { |s|
			sday = s.gsub(/(....)(..)(..)/, '\1-\2-\3')

			log "trial message for '#{jmp_number}' - sday #{sday}"

			"You are currently using the trial plan - see "\
				"https://jmp.chat/#pricing for details on the "\
				"included minutes and messages that you can "\
				"use until your trial expires (you signed up "\
				"on #{sday} - it expires 30 days after that); "\
				"note that if you upgrade to a paid account "\
				"(per https://jmp.chat/#upgrade instructions) "\
				"it may take up to 24 hours to show up here"
		}
	end

	def self.plan_info(jmp_number)
		jid = ''
		REDIS.get('catapult_jid-' + jmp_number).then { |j|
			jid = j
			log "using JID #{jid} for user of number above"

			today = Date.today

			# TODO: should ensure this is TAI (or UTC if TAI hard)
			last_month = today.prev_month.strftime('%Y%m')
			this_month = today.strftime('%Y%m')
			next_month = today.next_month.strftime('%Y%m')

			log "last month, this month, next month: "\
				"#{last_month}, #{this_month}, #{next_month}"

			last_key_key = "payment-plan_as_of_#{last_month}-#{jid}"
			this_key_key = "payment-plan_as_of_#{this_month}-#{jid}"
			next_key_key = "payment-plan_as_of_#{next_month}-#{jid}"
			REDIS.mget(last_key_key, this_key_key, next_key_key)
		}.then { |(last_plan_key, this_plan_key, next_plan_key)|
			if this_plan_key.nil?
				# might be at start of month and payment script
				#  hasn't finished running yet (which would copy
				#  last to this) so short-circuit the copy here

				log "this nil so use #{last_plan_key}"
				this_plan_key = last_plan_key
			end

			if next_plan_key.nil?
				# if user hasn't explicitly set a plan for next
				#  month, it will be this month's (per script ^)

				log "next nil so use #{this_plan_key}"
				next_plan_key = this_plan_key
			end

			if this_plan_key.nil?
				log "no plan key for '#{jid}' - on trial plan?"

				# TODO: once jmp-register sets a plan value,
				#  switch to the other commented text below
				trial_text(jmp_number).then { |text|
					text
				}

				#"Your account does not have a plan specified -"\
				#	" please contact support at xmpp:"\
				#	"#{SUPPORT_JID} to fix this"
			elsif PLANS.key? this_plan_key and
				PLANS.key? next_plan_key

				log "send this, next: #{this_plan_key}, "\
					"#{next_plan_key}"

				to_return = current_plans_text(this_plan_key,
					next_plan_key)

				to_return += "\n\n"

				payment_info(jid).then { |payment_details|
					to_return += payment_details

				# TODO: add list of available plans to
				#  switch to, after the above; use available_to!

				# TODO: include instructions for how to change
				#  plan if desired

					to_return + ''
				}
			else
				log "at least one of '#{this_plan_key}' or "\
					"'#{next_plan_key} for '#{jid}' !found"

				"It looks like the plan specified for your "\
					"account is no longer valid - please "\
					"contact support at xmpp:"\
					"#{SUPPORT_JID} to fix this"
			end
		}
	end

	def self.jingle_forward_info(jmp_number)
		jid = ''
		fwd = ''
		s = ''
		REDIS.mget('catapult_jid-' + jmp_number,
			'catapult_fwd-' + jmp_number).then { |j, f|

			jid = j
			fwd = f
			log "using jid/fwd val '#{jid}'/'#{fwd}' for user of ^#"

			# TODO: do actual escaping instead of just substitution
			default_fwd = 'sip:' +
				jid.split('@', 2)[0].sub('\\', '~') +
				# TODO: don't hard-code (pull from sgx-* params)
				'@jmp.bwapp.bwsip.io'

			# TODO: do actual escaping instead of just substitution
			jingle_fwd = 'sip:' +
				jid.split('@', 2)[0].sub('\\', '%5C') +
				'%40cheogram.com@sip.cheogram.com'

			if fwd == default_fwd
				s = 'JMP SIP account (the default - "disabled")'
			elsif fwd == jingle_fwd
				s = 'Jingle (Conversations, etc. receives them)'
			else
				s = 'a custom phone number or SIP address ( ' +
					fwd +
					' ); contact support if need to change'
			end

			log "forwarding status: '#{s}' for JID #{jid}"
			[s, jid]
		}
	end

	def self.jingle_forward_set(jmp_number, params)
		if params != 'enable' and params != 'disable'
			log "'#{params}' invalid for JMP number #{jmp_number}"
			return "Invalid param: '#{params}'; use enable/disable"
		end

		old_s = ''
		jid = ''

		jingle_forward_info(jmp_number).then { |s, j|
			old_s = s
			jid = j

			if old_s == 'JMP SIP account (the default - "disabled")'
				if params == 'disable'
					log "already #{params}d 4 #{jmp_number}"
					next 'Your account is already set to ' +
						'forward to the JMP SIP account'
				end

				# TODO: do actual escaping not just substitution
				jingle_fwd = 'sip:' +
					jid.split('@', 2)[0].sub('\\', '%5C') +
					'%40cheogram.com@sip.cheogram.com'

				REDIS.set('catapult_fwd-' + jmp_number,
					jingle_fwd).then { |rv|

					# TODO check rv
					log "Jingle #{params}d w/ rv #{rv} 4 " +
						jmp_number
					'Your account is now set to forward ' +
						'incoming calls to Jingle'
				}
			elsif old_s ==
				'Jingle (Conversations, etc. receives them)'

				if params == 'enable'
					log "already #{params}d 4 #{jmp_number}"
					next 'Your account is already set to ' +
						'forward calls to Jingle'
				end

				# TODO: do actual escaping not just substitution
				default_fwd = 'sip:' +
					jid.split('@', 2)[0].sub('\\', '~') +
					# TODO: don't hard-code; pull from sgx-*
					'@jmp.bwapp.bwsip.io'

				REDIS.set('catapult_fwd-' + jmp_number,
					default_fwd).then { |rv|

					# TODO check rv
					log "Jingle #{params}d w/ rv #{rv} 4 " +
						jmp_number
					'Your account is now set to forward ' +
						'calls to the JMP SIP account'
				}
			else
				log "cannot set to #{params} for #{jmp_number}"
				'Custom forwarding in use - cannot edit ' +
					'here; please contact support to edit'
			end
		}
	end

	def self.transcription_info(jmp_number)
		jid = ''
		REDIS.get('catapult_jid-' + jmp_number).then { |j|
			jid = j
			log "using JID #{jid} for user of number above"

			REDIS.getbit('catapult_setting_flags-' + jid,
				CatapultSettingFlagBits::VOICEMAIL_TRANSCRIPTION_DISABLED)
		}.then { |oldb|

			s = ''
			if 1 == oldb
				s = 'disable'
			else
				s = 'enable'
			end

			log "displaying '#{s}' for JID #{jid}"
			s
		}
	end

	def self.transcription_set(jmp_number, params)
		bit = 0
		if params == 'enable'
			bit = 0
		elsif params == 'disable'
			bit = 1
		else
			log "'#{params}' invalid for JMP number #{jmp_number}"
			return "Invalid param: '#{params}'; use enable/disable"
		end

		old_s = ''

		transcription_info(jmp_number).then { |s|
			old_s = s
			REDIS.get('catapult_jid-' + jmp_number)
		}.then { |j|
			jid = j
			log "using JID #{jid} for user of number above"

			REDIS.setbit('catapult_setting_flags-' + jid,
				CatapultSettingFlagBits::VOICEMAIL_TRANSCRIPTION_DISABLED,
				bit)
		}.then { |result|
			# TODO: use result (should be same as value of bit)

			log "#{params} transcr'n for JID #{jid}; was #{old_s}"
			"Voicemail transcription was #{old_s}d, now #{params}d"
		}
	end

	def self.cnam_get(jmp_number)
		EM::HttpRequest.new(
			'https://dashboard.bandwidth.com/api/tns/' +
				jmp_number[2..-1] + '/tndetails'
		).public_send(
			:get,
			head: {
				'Authorization' => [V2_USER, V2_PASS]
			},
                        body: nil
		).then { |http|
			puts "API response to send: #{http.response} with code"\
				" response.code #{http.response_header.status}"

			case http.response_header.status
			when 404
				puts 'CNAM error: 404'
				'cannot be set/accessed; please contact support'
			when 200
				xmldetail = Nokogiri::XML(http.response.to_s)

				pth = xmldetail.xpath("//SubscriberInformation")
				if pth.empty?
					puts 'CNAM ok: not set'
					'is not set'
				else
					status = xmldetail.xpath(
						"//Lidb//Status")
					if status.empty?
						puts 'CNAM error: no status'
						'is set to "' + pth[0].text +
							'" with no status (odd)'
					elsif status[0].text == 'Pending'
						puts 'CNAM ok: pending with "' +
							pth[0].text + '"'
						'has an update pending: "' +
							pth[0].text +
							'" is the requested '\
							'value - you should '\
							'see it set within 24 '\
							'hours'
					elsif status[0].text == 'Success'
						puts 'CNAM ok: success with "' +
							pth[0].text + '"'
						'is set to "' + pth[0].text +
							'"'
					else
						puts 'CNAM ok: unknown with "' +
							pth[0].text + '", s "' +
							status[0].text + '"'
						'is set to "' + pth[0].text +
							'" with odd status "' +
							status[0].text + '"'
					end
				end
			else
				puts 'CNAM error: other (see above)'
				'could not be retrieved due to error ' +
					http.response_header.status.to_s +
					'; please contact support to fix this'
			end
		}
	end

	def self.cnam_set(jmp_number, params)
		cnam_get(jmp_number).then { |result|
			if result != 'is not set'
				puts 'CNAM SET error with "' + params + '"'
				next 'CNAM already set or error while '\
					'accessing; please contact support'
			end

			# TODO: use different kind of escape so spaces untouched
			esc_params = WEBrick::HTTPUtils.escape(params)
			esc_params = esc_params.sub('%20', ' ')

			if esc_params.length > 15
				puts 'CNAM SET too long: "' + params + '" is ' +
					esc_params.length.to_s + ' char; e: "' +
					esc_params + '"'
				next 'Cannot set: CNAM must be 15 characters '\
					'or less but escaped provided value (' +
					esc_params + ') is ' +
					esc_params.length.to_s + ' characters'
			end

			order_xml = <<-eox
<LidbOrder>
    <LidbTnGroups>
        <LidbTnGroup>
            <TelephoneNumbers>
                <TelephoneNumber>#{jmp_number[2..-1]}</TelephoneNumber>
            </TelephoneNumbers>
            <SubscriberInformation>#{esc_params}</SubscriberInformation>
            <UseType>RESIDENTIAL</UseType>
            <Visibility>PUBLIC</Visibility>
        </LidbTnGroup>
    </LidbTnGroups>
</LidbOrder>
			eox

			EM::HttpRequest.new(
				'https://dashboard.bandwidth.com/api/accounts/'\
				+ V2_ACCT + '/lidbs'
			).public_send(
				:post,
				head: {
					'Authorization' => [V2_USER, V2_PASS],
					'Content-Type' => 'application/xml'
				},
	                        body: order_xml
			).then { |http|
				puts "API response to send: #{http.response} "\
					"with code response.code " +
					http.response_header.status.to_s

				resxml = Nokogiri::XML(http.response.to_s)

				if 201 == http.response_header.status
					pth = resxml.xpath("//ProcessingStatus")

					if not pth.empty? and pth[0].text ==
						'FAILED'

						puts 'CNAM SET failed status!'
						'Automated setting of CNAM "' +
							esc_params + '" failed'\
							' - contact support in'\
							' order to set the CNAM'
					else
						puts 'CNAM SET success!'
						'Request for CNAM of "' +
							esc_params + '" sent; '\
							'it should be set in '\
							'the next 24 hours - '\
							'contact support if not'
					end
				else
					puts 'CNAM SET fail with non-201!'
					'Could not submit request - please '\
						'contact support with code ' +
						http.response_header.status.to_s
				end
			}
		}
	end

	def self.process_command(creds, text)
		log "got '#{text}' from user of '#{creds.last}'"

		command, params = text.split(' ', 2)

		command = '' if command.nil?
		params = '' if params.nil?

		command.downcase!
		params.strip!

		case command
		when 'a'
# TODO:
## implement command to add to prepaid balance, since all plans "prepaid" crntly
			log "NI: command '#{command}' with params '#{params}'"
			"Adding funds is not yet implemented - to be notified "\
				"when it's ready to use, join our group chat "\
				"at xmpp:discuss@conference.soprani.ca?join "\
				"or our email notification list at "\
				"https://jmp.chat/sp1a/notify_signup/"
		when 'b'
# TODO:
## implement command to show prepaid balance (and which currency it's in)
### include balance after this month's charges: give everyone $2.99 bal for now?
#### actually show $3.00 for those with annual subscription (after-bonus rate)
### note that balance currently cannot be added to (not implemented); 'll + latr
			log "NI: command '#{command}' with params '#{params}'"
			"Showing balance not yet implemented - to be notified "\
				"when it's ready to use, join our group chat "\
				"at xmpp:discuss@conference.soprani.ca?join "\
				"or our email notification list at "\
				"https://jmp.chat/sp1a/notify_signup/"
		when 'c'
# TODO:
## implement command to change to different plan; MUST warn: not auto-unsubscrbd
			log "NI: command '#{command}' with params '#{params}'"
			"Changing plans not yet implemented - to be notified "\
				"when it's ready to use, join our group chat "\
				"at xmpp:discuss@conference.soprani.ca?join "\
				"or our email notification list at "\
				"https://jmp.chat/sp1a/notify_signup/"
		when 'i'
# TODO:
## implement command to show receipt for given month (auto-grab usage and $2.99)
### should also show bill-so-far for this month (for old months' say "paid")
#### i.e. "provisional invoice"
			log "NI: command '#{command}' with params '#{params}'"
			"Invoice feature not yet implemented - to be notified "\
				"when it's ready to use, join our group chat "\
				"at xmpp:discuss@conference.soprani.ca?join "\
				"or our email notification list at "\
				"https://jmp.chat/sp1a/notify_signup/"
		when 'j'
			if params.empty?
				jingle_forward_info(creds.last).then { |s, j|
				 'Incoming calls are being forwarded to ' + s
				}
			else
				jingle_forward_set(creds.last, params)
			end
		when 'm'
			if params.empty?
				cnam_get(creds.last).then { |result|
					'Your outgoing Caller ID ' + result
				}
			else
				cnam_set(creds.last, params)
			end
		when 'n'
			num_to_print = ''

			if creds.last.start_with? '+1' and
				creds.last.length == 12

				num_to_print = creds.last.gsub(
					/(..)(...)(...)(....)/, '\1 \2 \3 \4')
			else
				# TODO: pretty print other country codes' #s too
				num_to_print = creds.last
			end

			log "for command '#{command}' sending '#{num_to_print}'"
			'Your JMP number is ' + num_to_print
		when 'p'
			#plan_info(creds.last)
			# TODO: uncomment above to replace below once data ready
			log "NI: command '#{command}' with params '#{params}'"
			"Plan info feature not yet implemented - please "\
				"contact support at xmpp:#{SUPPORT_JID} to "\
				"see when your current plan expires; to learn "\
				"when this bot feature is ready to use, join "\
				"us at xmpp:discuss@conference.soprani.ca?join"\
				" or our email notification list at "\
				"https://jmp.chat/sp1a/notify_signup/"
		when 'r'
			if params.empty?
				transcription_info(creds.last).then { |s|
					"Your voicemail transcription is #{s}d"
				}
			else
				transcription_set(creds.last, params)
			end
		when 't'
# TODO:
## implement command to check porting status (MUST put this in proper todo file)
### port_status-JID would be populated by porting3 and manually updated by admin
### initial value might be "received by JMP at [time] but not yet submitted"
			log "NI: command '#{command}' with params '#{params}'"
			"Porting status not yet implemented - to be notified "\
				"when it's ready to use, join our group chat "\
				"at xmpp:discuss@conference.soprani.ca?join "\
				"or our email notification list at "\
				"https://jmp.chat/sp1a/notify_signup/"
		when 'u'
			# TODO: add YYYY-MM-DD support per
			#if params and params.length == 8 or params.length == 10

			# TODO: more indentation
			REDIS.get('usage_start_day-' + creds.last).then { |sday|

			if not sday or sday.length < 8
				log "sday of '#{sday.to_s}' missing/invalid"

				next "Your account is missing a start day so "\
					"we can't return usage stats - please "\
					"contact support at xmpp:"\
					"#{SUPPORT_JID} to fix this"
			else
				log "sday is '#{sday.to_s}' - seems valid"
			end

			if params.empty?
				td = Date.today
				d = td - 30
				d_str = d.strftime('%Y%m%d')

				if d_str < sday
					log "replace d (#{d_str}) with sday ("\
						"#{sday}) since d < sday"

					d = Date.parse(
						sday.gsub(/(....)(..)(..)/,
						'\1-\2-\3')
					)
					d_str = d.strftime('%Y%m%d')
				elsif d_str < STATS_BEGIN_DAY
					log "replace d (#{d_str}) with bday ("\
						"#{STATS_BEGIN_DAY}); d < bday"

					d = Date.parse(STATS_BEGIN_DAY.
						gsub(/(....)(..)(..)/,
						'\1-\2-\3')
					)
					d_str = d.strftime('%Y%m%d')
				end

				count = 0
				date_list = Array.new
				usage_keys = Array.new

				today = td.strftime('%Y%m%d')

				while d_str <= today do
					date_list.push(d_str)
					usage_keys.push('usage_minutes-' +
						d_str + '-' + creds.last)
					usage_keys.push('usage_messages-' +
						d_str + '-' + creds.last)
					d = d.next_day
					d_str = d.strftime('%Y%m%d')
					count += 1
				end

				log "usage - today: #{today}, count: #{count}"

				to_return = "Usage for last #{count} days"
				if count < 31
					# TODO: different msg if STATS_BEGIN_DAY
					to_return += " (less than 31 days as "\
						"account is newer than that)"
				end
				to_return += ":\n"

				REDIS.mget(*usage_keys).then { |use_vals|
					total_mins = 0
					total_msgs = 0
					for i in 0..count-1
						mins = use_vals[2*i] ?
							use_vals[2*i].to_i : 0
						msgs = use_vals[2*i+1] ?
							use_vals[2*i+1].to_i : 0

						total_mins += mins
						total_msgs += msgs

						to_return += date_list[i] +
							" - #{mins} minute(s),"\
							" #{msgs} sent "\
							"message(s)\n"
					end

					to_return += "\nTotal minutes: " +
						total_mins.to_s +
						', total outgoing messages: ' +
						total_msgs.to_s

					log "for command '#{command}' with "\
						"no params sent: #{to_return}"

					to_return
				}
			elsif params and params.length == 8 and
				params.gsub(/[^0-9]/, '').length == 8

				if params < sday
					log "params '#{params}'< sday '#{sday}'"

					next "The specified day (#{params}) "\
						"precedes the start day of "\
						"your JMP account (#{sday}) - "\
						"please try a different date"
				elsif params < STATS_BEGIN_DAY
					log "params '#{params}' < begin day "\
						"'#{STATS_BEGIN_DAY}'"

					next "The specified day (#{params}) "\
						"precedes the first day for "\
						"which stats are available ("\
						"#{STATS_BEGIN_DAY}) - "\
						"please try a different date"
				end

				pretty_params = params.gsub(/(....)(..)(..)/,
					'\1-\2-\3')

				messages = 0
				minutes = 0
				REDIS.get("usage_messages-#{params}-" +
					creds.last).then { |msgs|

					if msgs
						messages = msgs
					end

					REDIS.get("usage_minutes-#{params}-" +
						creds.last)
				}.then { |mins|

					if mins
						minutes = mins
					end

					log "for command '#{command}' sending "\
						"'#{messages}', '#{minutes}'"

					"You sent #{messages} message(s) and "\
						"used a total of #{minutes} "\
						"minute(s) on #{pretty_params}"
				}
			elsif params and params.length >= 16 and
				params[0..7].gsub(/[^0-9]/, '').length == 8 and
				params[-8,8].gsub(/[^0-9]/, '').length == 8

				# TODO: prettify, per below
				#from_date = Date.parse(params[0..7].gsub(
				#	/(....)(..)(..)/, '\1-\2-\3'))
				#to_date = Date.parse(params[-8,8].gsub(
				#	/(....)(..)(..)/, '\1-\2-\3'))

				from_date = params[0..7]
				to_date = params[-8,8]

				if to_date < from_date
					log "for command '#{command}' with "\
						"from/to of #{from_date}/"\
						"#{to_date} - range invalid"

					next "It looks like the 'from' date "\
						"of #{from_date} is after the "\
						"to date of #{to_date} - try "\
						"again with the dates reversed"
				end

				# NOTE: latter expr unneeded but keep to be safe
				if from_date < sday or to_date < sday
					log "'#{from_date}/#{to_date}' <#{sday}"

					next "The from date (#{from_date}) "\
						"and/or to date (#{to_date}) "\
						"precedes the start day of "\
						"your JMP account (#{sday}) - "\
						"please try a different date(s)"
				elsif from_date < STATS_BEGIN_DAY or
					to_date < STATS_BEGIN_DAY

					log "'#{from_date}/#{to_date}' < " +
						STATS_BEGIN_DAY

					next "The from date (#{from_date}) "\
						"and/or to date (#{to_date}) "\
						"precedes the first day for "\
						"which stats are available ("\
						"#{STATS_BEGIN_DAY}) - "\
						"please try a different date(s)"
				end

				count = 0
				d = Date.parse(from_date.gsub(/(....)(..)(..)/,
					'\1-\2-\3'))
				d_str = from_date
				date_list = Array.new
				usage_keys = Array.new

				while d_str <= to_date and count < 31 do
					date_list.push(d_str)
					usage_keys.push('usage_minutes-' +
						d_str + '-' + creds.last)
					usage_keys.push('usage_messages-' +
						d_str + '-' + creds.last)
					d = d.next_day
					d_str = d.strftime('%Y%m%d')
					count += 1
				end

				REDIS.mget(*usage_keys).then { |use_vals|
					to_return = "Usage for date range (up "\
						"to 31 days shown at a time - "\
						"#{count} day(s) shown here):\n"
					total_mins = 0
					total_msgs = 0
					for i in 0..count-1
						mins = use_vals[2*i] ?
							use_vals[2*i].to_i : 0
						msgs = use_vals[2*i+1] ?
							use_vals[2*i+1].to_i : 0

						total_mins += mins
						total_msgs += msgs

						to_return += date_list[i] +
							" - #{mins} minute(s),"\
							" #{msgs} sent "\
							"message(s)\n"
					end

					to_return += "\nTotal minutes: " +
						total_mins.to_s +
						', total outgoing messages: ' +
						total_msgs.to_s

					log "for command '#{command}' with "\
						"from/to of #{from_date}/"\
						"#{to_date} sent: #{to_return}"

					to_return
				}
			elsif params
				log "for command '#{command}' sending 'unsure'"\
					" due to param '#{params}'"

				"Not sure which day you mean; try 'u YYYYMMDD'"\
					" - for example, 'u 20180411'"
			else
				log "for command '#{command}' sending 'unsure'"\
					" due to unknown params"

				"Hmmm, we didn't get that; try 'u YYYYMMDD'"\
					" - for example, 'u 20180411'"
			end
			}
		else
			log "usage string to #{creds.last} since text '#{text}'"

			"This bot is deprecated and will go away soon. " \
				"Prefer to talk to xmpp:cheogram.com\n\n" \
				"Here are some commands"\
				" you can use to check or modify your account "\
				"(running command by itself with no parameters"\
				" (i.e. 'a') is always read-only):"\
				"\nj [enable/disable] - show/configure "\
				"Jingle forwarding setting; if enabled, your "\
				"calls will be sent to your XMPP client via "\
				"Jingle (supported by Conversations, etc.); "\
				"if not enabled (the default) incoming calls "\
				"will go to your JMP SIP account instead"\
				"\nm [full name] - show/set the outbound "\
				"Caller ID (CNAM) for your JMP number; note "\
				"that this can only be set once, is limited "\
				"to 15 characters, and 'should be the name of "\
				"the end customer (e.g., John Doe or John and "\
				"Jane Doe)' otherwise our carrier may not "\
				"accept the value - setting this is optional"\
				"\nn - display your JMP number"\
				"\nr [enable/disable] - show/configure "\
				"voicemail transcription setting"\
				"\nu [YYYYMMDD [YYYYMMDD]] - check usage for "\
				"given day, range, or last 31 days (no params)"\
				"\n\nThis bot's source code (AGPLv3+) is at "\
				"https://gitlab.com/ossguy/jmp-acct_bot - (C) "\
				"2018-2020 Denver Gingerich and others"\
				"\n\nIf you want to chat with a human, you can"\
				" reach JMP support at xmpp:#{SUPPORT_JID}"
		end
	end

	message :body do |m|
		log "<<< received message stanza (with body) ==>"
		puts m.inspect
		STDOUT.flush
		log "<== end of message stanza (with body)"

		fetch_catapult_cred_for(m.from).then { |creds|
			if not creds or creds.empty?
				log "got '#{m.body}' from user JID '#{m.from}'"

				reply = m.reply
				reply['id'] = SecureRandom.uuid

				reply.body =
				"Hi!  I'm the JMP account bot.  It looks like "\
				"you don't have a JMP account yet - you can "\
				"get one from https://jmp.chat/ and then "\
				"message me again for a list of commands."\
				"\n\nThis bot's source code (AGPLv3+) is at "\
				"https://gitlab.com/ossguy/jmp-acct_bot - (C) "\
				"2018-2020 Denver Gingerich and others"\
				"\n\nIf you want to chat with a human, you can"\
				" reach JMP support at xmpp:#{SUPPORT_JID} - "\
				"we normally respond within 24 hours, but "\
				"usually much sooner"

				log "<<< sending reply to non-JMP user ==>"
				puts reply.inspect
				STDOUT.flush
				log "<== end of reply to non-JMP user"

				write_to_stream reply
				next
			else
				process_command(creds, m.body)
			end
		}.then { |result|
			reply = m.reply
			reply['id'] = SecureRandom.uuid
			reply.body = result
			write_to_stream reply
		}.catch { |e|
			t = Time.now
			log "ERROR: #{e.inspect}"

			reply = m.reply
			reply['id'] = SecureRandom.uuid
			reply.body = "We encountered an error processing your "\
				"message - please contact support at xmpp:"\
				"#{SUPPORT_JID} to fix this, and include the "\
				"timestamp '#{t.to_i.to_s}' in your message"
			write_to_stream reply
		}
	end

	message do |m|
		log "<<< received message stanza (without body so ignoring) ==>"
		puts m.inspect
		STDOUT.flush
		log "<== end of message stanza (without body so ignoring)"
        end

	def self.user_cap_identities
		[]
	end

	def self.user_cap_features
		[]
	end

	subscription :request? do |p|
		puts "PRESENCE1: #{p.inspect}"

		# subscriptions are allowed from anyone - send reply immediately
		msg = Blather::Stanza::Presence.new
		msg.to = p.from
		msg.from = p.to
		msg.type = :subscribed

		puts 'RESPONSE5a: ' + msg.inspect
		write_to_stream msg

		# send a <presence> immediately; not automatically probed for it
		# TODO: refactor so no "presence :probe? do |p|" duplicate below
		caps = Blather::Stanza::Capabilities.new
		# TODO: user a better node URI (?)
		caps.node = 'http://account.bot.jmp.chat/'
		caps.identities = user_cap_identities
		caps.features = user_cap_features

		msg = caps.c
		msg.to = p.from
		msg.from = p.to.to_s + '/acct_bot'

		puts 'RESPONSE5b: ' + msg.inspect
		write_to_stream msg

		# need to subscribe back so Conversations displays images inline
		msg = Blather::Stanza::Presence.new
		msg.to = p.from.to_s.split('/', 2)[0]
		msg.from = p.to.to_s.split('/', 2)[0]
		msg.type = :subscribe

		puts 'RESPONSE5c: ' + msg.inspect
		write_to_stream msg
	end

	presence :probe? do |p|
		puts 'PRESENCE2: ' + p.inspect

		caps = Blather::Stanza::Capabilities.new
		# TODO: user a better node URI (?)
		caps.node = 'http://account.bot.jmp.chat/'
		caps.identities = user_cap_identities
		caps.features = user_cap_features

		msg = caps.c
		msg.to = p.from
		msg.from = p.to.to_s + '/acct_bot'

		puts 'RESPONSE6: ' + msg.inspect
		write_to_stream msg
	end

        presence do |p|
                log "<<< received presence stanza ==>"
                puts p.inspect
		STDOUT.flush
                log "<== end of presence stanza"
        end

        iq do |i|
                log "<<< received iq stanza ==>"
                puts i.inspect
		STDOUT.flush
                log "<== end of iq stanza"
        end
end

EM.run do
	REDIS = EM::Hiredis.connect
	AccountBot.run
end
